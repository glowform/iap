//Big main logo animation  
var animation = bodymovin.loadAnimation({
    container: document.getElementById('main-logo-anim'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'js/main-logo-anim.json'
})
//First section animations
$('#top .top-title, #top .heading-title-big, #top .description-slide, #top a.btn').addClass('animated fadeInDown');
$('#top a.btn').addClass('hover-anim');
$(window).load(function () {
    setTimeout(function () {
        $('#top a.btn').removeClass('animated fadeInDown')
    }, 3200);
});
//Vivus svg icon animations
new Vivus('svg_icon1', {
    type: 'delayed',
    delay: 70,
    start: 'inViewport', 
    duration: 160,
    animTimingFunction: Vivus.EASE
});
new Vivus('svg_icon2', {
    type: 'delayed',
    delay: 70,
    start: 'inViewport', 
    duration: 160,
    animTimingFunction: Vivus.EASE
});
new Vivus('svg_icon3', {
    type: 'delayed',
    delay: 70,
    start: 'inViewport', 
    duration: 160,
    animTimingFunction: Vivus.EASE
});
new Vivus('svg_icon4', {
    type: 'delayed',
    delay: 70,
    start: 'inViewport', 
    duration: 160,
    animTimingFunction: Vivus.EASE
});
new Vivus('svg_icon5', {
    type: 'delayed',
    delay: 70,
    start: 'inViewport', 
    duration: 160,
    animTimingFunction: Vivus.EASE
});
new Vivus('svg_icon6', {
    type: 'delayed',
    delay: 70,
    start: 'inViewport', 
    duration: 160,
    animTimingFunction: Vivus.EASE
});
