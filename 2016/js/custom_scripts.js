	
var customJS;

jQuery(document).ready(function($){

	customJS = {
		
		common : {
			commonJS : function(){
				
				//
				
			},
			
			html5Tags : function(){
				document.createElement('header');  
				document.createElement('section');  
				document.createElement('nav');  
				document.createElement('footer');  
				document.createElement('menu');  
				document.createElement('hgroup');  
				document.createElement('article');  
				document.createElement('aside');  
				document.createElement('details'); 
				document.createElement('figure');
				document.createElement('time');
				document.createElement('mark');
			},
						
			commonInput : function(){								
				 $('body').on('click','.iconMobNav', function(){
			        $(this).toggleClass('open');
				 });
			    
			     $('body').on('click','.iconMobNav', function(){
			       	if(!$('.pageNav ul').is(':visible')){
				       	$('.pageNav ul').toggleClass('show');
				      }else{
				      		$('.pageNav ul').removeClass('show');
				      }
			    });
			    
			    
			   
			    
			    var controller = new ScrollMagic.Controller();
			    
			   	//Logo
			    	var splitText = new SplitText(".bannerContent h1", {type:"words,chars"});
			    	TweenLite.set(".bannerContent h1", {perspective:400});
			    	
				     var logo = new TimelineMax()
				     				.staggerFrom('.logoOrangeLine', 2, {rotation:-180, scale:0, ease: Expo.easeOut}, 1,"lines" )
				     				.staggerFrom('.logoWhiteLine', 2, {rotation:101,scale:0,  ease: Expo.easeOut},1,"lines+=0.5" )
				     				.staggerFrom('.logoBlueLine', 2, {rotation:-180,scale:0, ease: Expo.easeOut}, 1,"lines+=0.9" )
				     				
				     				.staggerFrom('.logoOrangeRound', 1, {rotation:360, scale:0, ease: Expo.easeOut}, 1,"lines+=1.1")
				     				.staggerFrom('.logoBlueRound', 1, {rotation:360, scale:0, ease: Expo.easeOut},  1,"lines+=1.3" )
				     				.staggerFrom('.logoWhiteRound',1, {rotation:360, scale:0, ease: Expo.easeOut}, 1,"lines+=1.4" )
				     				
				     				.staggerFrom('.logoIndependent', 1, { x:-100,opacity:0, ease: Expo.easeOut}, 1,"lines+=2" )
				     				.staggerFrom('.logoAccountability', 1, {x:-100,opacity:0, ease: Expo.easeOut},1,"lines+=2.2" )
				     				.staggerFrom('.logoPanel', 1, {x:-100, opacity:0, ease: Expo.easeOut}, 1,"lines+=2.4" )
				     				.staggerFrom('.logoOrangeText', 1, {x:-100, opacity:0, ease: Expo.easeOut}, 1,"lines+=2.6" )
				     				.staggerFromTo('.bannerSection', 1, {backgroundColor:'#6397CD', ease: Expo.easeOut},{backgroundColor:'none', ease: Expo.easeOut},1,"lines+=2.8" )
									.staggerFrom('.headerRight li.report', 0.8, {y:+100,opacity:0 ,ease: Expo.easeOut},0.5,"lines+=3" )
									.staggerFrom('.headerRight li.summary', 0.8, {y:+100,opacity:0 ,ease: Expo.easeOut},0.5,"lines+=3.2" )
									.staggerFrom('.headerRight li.socialIcon', 0.8, {y:+100,opacity:0 ,ease: Expo.easeOut},0.5,"lines+=3.4" )
				     				.staggerFrom(splitText.chars,1, {opacity:0, scale:0, x:80, transformOrigin:"0% 50% -50",  ease:Back.easeOut}, 0.02,"lines+=3.6" )
				    				.from('.bannerContent article', 1, {y: "-50px",opacity:0, ease:Expo.easeOut},"lines+=4.2");
				     var scene = new ScrollMagic.Scene({ triggerElement:'.logoWrapper'})
				    .setTween(logo)
					.triggerHook(1)
					//.addIndicators() 
					.addTo(controller);
					
			 

			    //Banner Bottom Text
			    			
					var bannerBotton = new TimelineLite, 
			    	splitText = new SplitText(".nowTheTimwe .pageCenter", {type:"words,chars"}) ; 
			
					TweenLite.set(".nowTheTimwe .pageCenter", {perspective:400});
			
					bannerBotton.staggerFrom(splitText.chars,0.8, {opacity:0, scale:0, y:80, rotationX:180, transformOrigin:"0% 50% -50",  ease:Back.easeOut}, 0.02);
					 var scene = new ScrollMagic.Scene({triggerElement: ".nowTheTimwe"})
					.setTween(bannerBotton)
					.triggerHook(0.5)
					//.addIndicators() 
					.addTo(controller);
					
					
				//Whatershed Year
					
					var watershed = new TimelineLite, 
						splitH2 = new SplitText(".waterShedContent h2", {type:"words,chars"}),
					    splitText = new SplitText(".waterShedContent em", {type:"words,chars"});
					    
					TweenLite.set(".waterShedContent h2", {perspective:400});
					
					function random(min, max){
						return (Math.random() * (max - min)) + min;
					}
					watershed.staggerFrom(splitH2.chars,1, {opacity:0, scale:0, x:80, transformOrigin:"0% 50% -50",  ease:Back.easeOut}, 0.02 );
					$(splitText.chars).each(function(i){
					var watershedContent = TweenMax.from($(this),0.5 , {
							opacity: 0,
							x: random(-500, 500),
							y: random(-500, 500),
							z: random(-500, 500),
							scale: .1,
							 delay: i * .02
						});
						var scene1 = new ScrollMagic.Scene({triggerElement: ".waterShedYer"})
									.setTween(watershedContent)
									.triggerHook(0.5)
									//.addIndicators() 
									.addTo(controller);
					});

					var scene = new ScrollMagic.Scene({triggerElement: ".waterShedYer"})
									.setTween(watershed)
									.triggerHook(0.5)
									//.addIndicators() 
									.addTo(controller);
				
				
				//WORLD OF INEQUALITIES
				
					var equalities = new TimelineLite;
			    	splitH2 = new SplitText(".equalities .pageCenter h2", {type:"words,chars"});
					TweenLite.set(".equalities .pageCenter h2", {perspective:400});
			
					equalities.staggerFrom(".equalities img",2,{rotation:-90,y:+300,opacity:0,ease: Back.easeOut},0.5,"equalities")
							  .staggerFrom(splitH2.chars,1, {opacity:0, scale:0, x:80, transformOrigin:"0% 50% -50",  ease:Back.easeOut}, 0.02,"equalities+=0.8" )
							  .staggerFrom(".equalities .pageCenter span",1.5, {opacity:0, y:+200,ease: Expo.easeOut},0.5,"equalities+=1.2")
					 		  .staggerFrom(".qualityListWrapepr .firstRow", 0.8,{y: +50, alpha: 0,opacity:0, ease: Expo.easeOut},0.5,"equalities+=1.4")
					 		  .staggerFrom(".qualityListWrapepr .secondRow", 0.8,{y: +50, alpha: 0,opacity:0, ease: Expo.easeOut}, 0.5,"equalities+=1.6")
					 		  .staggerFrom(".qualityListWrapepr .thirdRow", 0.8,{y: +50, alpha: 0,opacity:0, ease: Expo.easeOut}, 0.5,"equalities+=1.8")
					 		  .staggerFrom(".qualityListWrapepr .fourthRow", 0.8,{y: +50, alpha: 0,opacity:0, ease: Expo.easeOut}, 0.5,"equalities+=2")
					 		  .staggerFrom(".qualityListWrapepr .fifthRow", 0.8,{y: +50, alpha: 0,opacity:0, ease: Expo.easeOut}, 0.5,"equalities+=2.2");
					 var scene = new ScrollMagic.Scene({triggerElement: ".equalities"})
					.setTween(equalities)
					.triggerHook(0.5)
					//.addIndicators() 
					.addTo(controller);
					
				//HUMANITARIAN
					
					var humanitarian = new TimelineLite; 

			    	splitText = new SplitText(".humanitarianSettings .pageCenter h2",{type:"words,chars"});
					TweenLite.set(".humanitarianSettings .pageCenter h2", {perspective:400});
			
					humanitarian.from(".humanitarianSettings img",2,{scale: 2, ease: Sine.easeOut}, "humanitarian")
								.staggerFrom(splitText.chars,1, {opacity:0, scale:0, x:80, transformOrigin:"0% 50% -50",  ease:Back.easeOut}, 0.02,"humanitarian+=1" )
								.from('.humanitarianSettings .column', 1.5,{y: +200, alpha: 0,opacity:0, ease: Expo.easeOut},"humanitarian+=1.6");
					
					 var scene = new ScrollMagic.Scene({triggerElement: ".humanitarianSettings"})
					.setTween(humanitarian)
					.triggerHook(0.8)
					//.addIndicators() 
					.addTo(controller);
					
			 // ADOLESCENTS  
			
					var adolescents = new TimelineLite, 
			    	
					splitText = new SplitText(".adolescents .pageCenter h2",{type:"words,chars"});
					TweenLite.set(".adolescents .pageCenter h2", {perspective:400});
					
					adolescents.staggerFrom(splitText.chars,1, {opacity:0, scale:0, x:80, transformOrigin:"0% 50% -50",  ease:Back.easeOut}, 0.02,"adolescents" )
							    .from('.adolescents .column', 2.5,{y: -100, alpha: 0,opacity:0, ease: Elastic.easeOut},"adolescents+=0.8");
					 var scene = new ScrollMagic.Scene({triggerElement: ".adolescents"})
					.setTween(adolescents)
					.triggerHook(0.5)
					//.addIndicators() 
					.addTo(controller);	
					
			 // HealthSector  
			
					var healthSector = new TimelineLite; 
			    	splitText = new SplitText(".healthSector .pageCenter h2", {type:"words,chars"}) ; 
			
					TweenLite.set(".healthSector .pageCenter h2", {perspective:400});
			
					healthSector.staggerFromTo('.healthSector img',5,{rotation: 45},{rotation: 10,ease: Elastic.easeOut.config( 1, 0.5)},1,"healthSector")
								.staggerFrom(splitText.chars,1, {opacity:0, scale:0, x:80, transformOrigin:"0% 50% -50",  ease:Back.easeOut}, 0.02,"healthSector+=0.5")
							    .from('.healthSector .column', 2.5,{y: -50, alpha: 0,opacity:0, ease: Elastic.easeOut},"healthSector+=1");
					 var scene = new ScrollMagic.Scene({triggerElement: ".healthSector"})
					.setTween(healthSector)
					.triggerHook(0.5)
					//.addIndicators() 
					.addTo(controller);	
					
			//Heightened
				
					var heightened = new TimelineLite;
			    	splitText = new SplitText(".heightendSection h2", {type:"words,chars"}) ; 
			
					TweenLite.set(".heightendSection h2", {perspective:400});
			
					heightened.staggerFrom(splitText.chars,1, {opacity:0, scale:0, x:80, transformOrigin:"0% 50% -50",  ease:Back.easeOut}, 0.02)
							  .staggerFrom(".heightendListWrapepr .firstRow",0.8, {rotation:90,opacity:0, y:100,ease: Back.easeOut}, 0.5,"-=0.8")
							  .staggerFrom(".heightendListWrapepr .secondRow",0.8, {rotation:-90,opacity:0, y:100,ease: Back.easeOut},0.5,"-=1");
					 var scene = new ScrollMagic.Scene({triggerElement: ".heightendSection"})
					.setTween(heightened)
					.triggerHook(0.5)
					//.addIndicators() 
					.addTo(controller);
			
			//Roadmap
				
				//var counter = { var: 0 };
				var Roadmap = new TimelineLite; 

		    	splitText = new SplitText(".clearlyDefinedRoadmap .pageCenter h2",{type:"words,chars"});
				TweenLite.set(".clearlyDefinedRoadmap .pageCenter h2", {perspective:400});
		
				Roadmap.staggerFrom(splitText.chars,1, {opacity:0, scale:0, x:80, transformOrigin:"0% 50% -50",  ease:Back.easeOut}, 0.02)
					   .from('.clearlyDefinedRoadmap .pageCenter p', 1,{opacity:0, y:+300,ease: Expo.easeOut},"-=1.2" )
					   .staggerFrom(".clearlyDefineWrapper .clearlyDefineList",1, {rotation:90,opacity:0, y:100}, 1)
					   .to(".clearlyDefineList .commitments", 3, {scrambleText:{text:"177", chars:"012",  ease:Linear.easeNone}},"number")
					   .to(".clearlyDefineList .billion", 3, {scrambleText:{text:"25.5", chars:"0123",  ease:Linear.easeNone}},"number")
					   .from(".clearlyDefineWrapper .clearlyDefineList .iconCommitment",0.5, {rotation:360,ease: Elastic.easeOut.config( 1, 0.5)},"-=0.8" )
					   .from(".clearlyDefineWrapper .clearlyDefineList .iconFinancial",0.5, {rotation:360,ease: Elastic.easeOut.config( 1, 0.5)},"-=0.5")
					   .staggerFrom('.clearlyDefinedRoadmap .globalStrategy', 0.5,{opacity:0,alpha:0, y:"50px",ease: Expo.easeOut});
				
				 var scene = new ScrollMagic.Scene({triggerElement: ".clearlyDefinedRoadmap"})
				.setTween(Roadmap)
				.triggerHook(0.5)
				//.addIndicators() 
				.addTo(controller);
					
			//FurtherAction
			
				// function randomNum (min, max) {
			    	// return Math.random() * (max - min) + min;
				// } 
				var furtherAction = new TimelineLite; 

		    	splitText = new SplitText(".furtherActionContent h2",{type:"words,chars"});
				TweenLite.set(".furtherActionContent h2", {perspective:400});
		
				// $('.furtherActionContent h2').find("div").each(function(){
						  // furtherAction.fromTo(this,0.2, {autoAlpha:0, rotation:randomNum(-360, 360), rotationX:randomNum(-360, 360), rotationY:randomNum(-360, 360), rotationZ:randomNum(-360, 360), scale:0}, {autoAlpha:1, rotation:0, rotationX:0, rotationY:0, rotationZ:0, scale:1});
				 // });
				furtherAction.staggerFrom(splitText.chars,1, {opacity:0, scale:0, x:80, transformOrigin:"0% 50% -50",  ease:Back.easeOut}, 0.02,'action')
							 .staggerFrom(".furtherActionContent .leadership figure",1.5, { alpha: 0,rotation:360,opacity:0,scale:0,ease: Elastic.easeOut.config( 1, 0.75)},0.8,'action+=0.8')
							 .staggerFrom(".furtherActionContent .leadership h3",1, {y: +100, alpha: 0,opacity:0,ease: Expo.easeOut},0.8,'action+=0.8')
							 .staggerFrom(".furtherActionContent .leadership p",1, {y: +100, alpha: 0,opacity:0,ease: Expo.easeOut},0.8,'action+=0.8')
							 .staggerFrom(".furtherActionContent .resources figure",1.5, { alpha: 0,rotation:360,opacity:0,scale:0,ease: Elastic.easeOut.config( 1, 0.75)},0.8,'action+=1.4')
							 .staggerFrom(".furtherActionContent .resources h3",1, {y: +100, alpha: 0,opacity:0,ease: Expo.easeOut},0.8,'action+=1.4')
							 .staggerFrom(".furtherActionContent .resources p",1, {y: +100, alpha: 0,opacity:0,ease: Expo.easeOut},0.8,'action+=1.4')
							 .staggerFrom(".furtherActionContent .institutions figure",1.5, { alpha: 0,rotation:360,opacity:0,scale:0,ease: Elastic.easeOut.config( 1, 0.75)},0.8,'action+=1.8')
							 .staggerFrom(".furtherActionContent .institutions h3",1, {y: +100, alpha: 0,opacity:0,ease: Expo.easeOut},0.8,'action+=1.8')
							 .staggerFrom(".furtherActionContent .institutions p",1, {y: +100, alpha: 0,opacity:0,ease: Expo.easeOut},0.8,'action+=1.8')
				 var scene = new ScrollMagic.Scene({triggerElement: ".furtherAction"})
				.setTween(furtherAction)
				.triggerHook(0.5)
				//.addIndicators() 
				.addTo(controller); 
					 
					
			//Accountability
			
				var accountability = new TimelineLite; 
		    	splitText = new SplitText(".accountability .pageCenter h2", {type:"words,chars"}) ; 
		
				TweenLite.set(".accountability .pageCenter h2", {perspective:400});
		
				accountability.staggerFrom(splitText.chars,1, {opacity:0, scale:0, x:80, transformOrigin:"0% 50% -50",  ease:Back.easeOut}, 0.02)
						   	   .from(".accountability .pageCenter span", 1, {y: -100, alpha: 0, ease: Expo.easeOut},"-=0.6")
						   	   .staggerFrom(".accountabilityLeft .monitor h3 ",  1, {y: +100, alpha: 0,opacity:0,ease: Expo.easeOut},1,'stages')
						   	   .staggerFrom(".accountabilityLeft .monitor p ",  1, {y: +100, alpha: 0,opacity:0,ease: Expo.easeOut},1,'stages')
						   	   .staggerFrom(".accountabilityLeft .review h3 ",  1, {y: +100, alpha: 0,opacity:0,ease: Expo.easeOut},1,'stages+=0.5')
						   	   .staggerFrom(".accountabilityLeft .review p ",  1, {y: +100, alpha: 0,opacity:0,ease: Expo.easeOut},1,'stages+=0.5')
						   	   .staggerFrom(".accountabilityLeft .remedy h3 ",  1, {y: +100, alpha: 0,opacity:0,ease: Expo.easeOut},1,'stages+=0.8')
						   	   .staggerFrom(".accountabilityLeft .remedy p ",  1, {y: +100, alpha: 0,opacity:0,ease: Expo.easeOut},1,'stages+=0.8')
						   	   
			    			   .staggerFrom(".countryAccountability .imgGraph",2,{rotation: 360,scale:0,opacity: 0, ease: Back.easeOut},1,"graph1" )
			    			   .staggerFrom(".countryAccountability .imgAccountability", 1,{scale:0,opacity: 0, ease: Expo.easeOut},0.5,"graph1+=1.4" )
			    			   .staggerFrom(".countryAccountability .topAction", 1,{scale:0,opacity: 0, ease: Expo.easeOut},0.2,"graph1+=1.8" )
			    			   
			    			   .staggerFrom(".remediesAction .arrow", 0.2,{y: +50,opacity: 0,scale:0, alpha: 0, ease:Expo.easeOut},0.2,"arrow" )
			    			   .staggerFrom(".remediesAction .monitor",0.5,{scale:0,opacity: 0, ease: Expo.easeOut},0.2,"arrow+=0.2" )
			    			   
			    			   .staggerFrom(".regionalAccountability .imgRegionalGraph", 2,{rotation: 360,scale:0,opacity: 0, ease: Back.easeOut},1,"graph2" )
			    			   .staggerFrom(".regionalAccountability .imgAccountability", 1,{scale:0,opacity: 0, ease: Expo.easeOut},0.5,"graph2+=1.4" )
			    			   .staggerFrom(".regionalAccountability .bottomAction",1,{scale:0,opacity: 0, ease: Expo.easeOut},0.5,"graph2+=1.8" );
			    			   
			    			  
			    			   
				 var scene = new ScrollMagic.Scene({triggerElement: ".accountability"})
				.setTween(accountability)
				.triggerHook(0.5)
				//.addIndicators() 
				.addTo(controller);	
					
		// Footer
			    
			     
			     var Footer = new TimelineLite, 
		    		 splitText = new SplitText(".pageFooter .pageCenter span", {type:"words,chars"}) ; 
		
				TweenLite.set(".pageFooter .pageCenter span", {perspective:400});
				
			     Footer.staggerFrom(splitText.chars,0.6, {opacity:0, scale:0, y:80, rotationX:180, transformOrigin:"0% 50% -50",  ease:Back.easeOut}, 0.02)
			     	   .from(".pageFooter .contact", 0.3, {y:+200,scale: 0, alpha: 0,opacity: 0, ease: Expo.easeOut},"-=1");
			    var scene = new ScrollMagic.Scene({triggerElement: ".pageFooter"})
					.setTween(Footer)
					.triggerHook(0.8)
					//.addIndicators() 
					.addTo(controller);
				
							    
			    
			    
			    
			    $(window).scroll(function(){
			    	var scrollPos = $(document).scrollTop(),
				    getNavHeight = $('.pageNav').outerHeight()+2,
				    getWindowHeight = $(window).height();
				    winHref = location.hash;
				    $('.pageNav li span a').each(function () {
				        var currLink = $(this);
				        var refElement = currLink.attr("href"),
				        refElement = refElement.replace('#','');
				        
				        if ($('section[position="'+refElement+'"]').position().top-getNavHeight <= scrollPos && $('section[position="'+refElement+'"]').position().top-getNavHeight + $('section[position="'+refElement+'"]').height() > scrollPos) {
				            $('.pageNav li').removeClass("selected");
				            currLink.closest('li').addClass("selected");
				            location.hash = $('.pageNav li.selected a:eq(0)').attr('href');
				            $('.titleText').text($('.pageNav li.selected a:eq(0)').text());
				        }
				        else{
				           // currLink.closest('li').removeClass("selected");
				        }
				    });
				    if($('.pageNav li.selected a:eq(0)').attr('href')=="#Home"){
				    	$('.mobileLogo').addClass('showOnMobile').removeClass('hideOnMobile');
				    	$('.logoSmall, .titleText').addClass('hideOnMobile').removeClass('showOnMobile').removeClass('mobileTableCell');
				    }else{
				    	$('.mobileLogo').addClass('hideOnMobile').removeClass('showOnMobile');
				    	$('.logoSmall').addClass('showOnMobile').removeClass('hideOnMobile');
				    	$('.titleText').addClass('mobileTableCell').removeClass('hideOnMobile');
				    }
				    
				    getScrollPost();
				    
				   
			    });
			    
			    function getScrollPost(){
			    	if($('.pageNav li.selected').prevAll('li').size()>=1){
				    	$('.waterShedYerImg').addClass('fixed');
				    }else{
				    	$('.waterShedYerImg').removeClass('fixed');
				    }
			    }
			    getScrollPost();
			  $(window).trigger('scroll');
				
			    
			    $('.pageNav li a').on('click', function(){
			    	$(this).closest('ul').removeClass();
			    	$('.iconMobNav').removeClass('open');
			    });
			    
			    
			    
			    var currentScrollPos;
				$(".iconMobNav").on('click',function() {
					if($(this).hasClass('activeMenu')){
						$(this).removeClass('activeMenu');
						$('.pageNav').removeClass('headerScroll');
						$(".mobNav").removeClass('menuShow');
						$('html,body').stop().animate({scrollTop: currentScrollPos},{queue: false, duration:1000, easing:'easeOutExpo'});
					}else{
						$(this).addClass('activeMenu');
						$('.pageNav').addClass('headerScroll');
						currentScrollPos = $(window).scrollTop();
						$(".mobNav").addClass('menuShow');;
						$('html,body').stop().animate({scrollTop: 0},{queue: false, duration:1000, easing: 'easeOutExpo'});
					}
				});
				
				$(".mobileLogo").on('click',function() {
					currentScrollPos = 0;
					$(".iconMobNav").trigger('click');
				});
			    
			    
				 /* $('.pageNav li a').click(function() {
				  	
					
					//return false;
				    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				      var target = $(this.hash);
				      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				      if (target.length) {
				        $('html,body').animate({scrollTop: target.offset().top}, 1000);
				       // return false;
				      }
				    }
				  });*/
				  
				  $(".logoSmall a").click(function() {
					  $('html,body').stop().animate({scrollTop: 0},{queue: false, duration:1000, easing: 'easeOutExpo'});
					  return false;
					});
				  
				  
				  $('.pageNav li').hover(function(){
				   $('.pageNav li').each(function(){
				   		var offsetHeight = $(this).find('em').outerHeight();				   		
				  			$(this).find('em').css('top',-offsetHeight-30);
				  		});
				  });
				  		  
				  
				  function fixedHeader(){
					  var topHeight = $('.pageHeader').outerHeight() + $('.bannerArea').outerHeight()-75;				  
					  $(window).scroll(function() {		
					  	if ($(this).scrollTop() > topHeight){  
					        $('.pageNav').addClass("sticky");
					    }else{
					        $('.pageNav').removeClass("sticky");
					    }						
					  });
				  }
				  
				  fixedHeader();
				  
				  $(window).load(function() {
					fixedHeader();
				});
				
				$('.pageNav li a').on('click touchend',function(){
				    var titleText = $(this).text();
					$('.pageNav li span a, .pageNav li p a').each(function(){
						$('.titleText').text(titleText);
			     	});
			     	
			     	currentScrollPos=0;
					$('.pageNav').removeClass('headerScroll');
					$(".mobNav").removeClass('menuShow');
					$(".iconMobNav").removeClass('activeMenu');
					
					
					var getNavHeight = $('.pageNav').outerHeight(),
					getId = $(this).attr('href');
					if(getId.indexOf('#')!=-1){
						getId = getId.split('#'),
						idPosition = $('section[position="'+getId[1]+'"]').offset().top;
						$('html,body').animate({scrollTop: idPosition-getNavHeight}, {duration:1000, queue: false, easing:'easeOutExpo'});
					}
					$('.pageNav ul').removeClass('show');
				});
				
				var getLocation = window.location.href,
				getNavHeight = $('.pageNav').outerHeight();
				if(getLocation.indexOf('#')!=-1){
					var getId = getLocation.split('#'),
					idPosition = $('section[position="'+getId[1]+'"]').offset().top;
					$('html,body').animate({scrollTop: idPosition-getNavHeight}, {duration:1000, queue: false, easing:'easeOutExpo'});
				}
				
				
				$('.pageNav li span a').on('click', function(){
			     	$('.pageNav li').removeClass('selected');
			     	$(this).closest('li').addClass('selected');
			    });
			    
			    
			    
						    
				   
				
				var $inputText = $('.queryInput input, .queryInput textarea');
				$inputText.each(function(){
					var $thisHH = $(this);
					if(!$(this).val()){
						$(this).parent().find('label').show();
					}else{
						setTimeout(function(){
						$thisHH.parent().find('label').hide();
						},100);
					}
					
				});
				$inputText.focus(function(){
					if(!$(this).val()){
						$(this).parent().find('label').addClass('showLab');
					}
				});
				$inputText.keydown(function(){
					if(!$(this).val()){
						$(this).parent().find('label').hide();
					}
				});
				$inputText.on("blur",function(){
					var $thisH = $(this);
					if(!$(this).val()){
						$(this).parent().find('label').show().removeClass('showLab');
					}else{
						$thisH.parent().find('label').hide();
					}
					
				});
				
			}
		
		}//end commonJS
			
	};
	

	customJS.common.commonJS();
	customJS.common.html5Tags();
	customJS.common.commonInput();
	setTimeout(function(){
		$('.preloader').hide();
	},700);
	
	 
});
